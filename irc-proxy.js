var irc = require('irc');
var amqp = require('amqplib/callback_api');


var proxies = {}; // mapa de proxys
var amqp_conn;
var amqp_ch;
var irc_client;

// Conexão com o servidor AMQP
amqp.connect('amqp://localhost', function(err, conn) {
	conn.createChannel(function(err, ch) {
		amqp_conn = conn;
		amqp_ch = ch;

		inicializar();
	});
});

function inicializar () {

	receberDoCliente("registro_conexao", function (msg) {

		var id       = msg.id;
		var servidor = msg.servidor;
		var nick     = msg.nick;
		var canal    = msg.canal;

		irc_client = new irc.Client(
			servidor, 
			nick,
			{channels: [canal]}
		);

		irc_client.addListener('message'+canal, function (from, message) {
		    
		    console.log(from + ' => '+ canal +': ' + message);
		    
		    enviarParaCliente(id, {
		    	"timestamp": Date.now(), 
				"nick": from,
				"msg": message
			});
		});

		irc_client.addListener('registered', function(message){
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": nick,
				"msg": 'Bem vindo! Você foi registrado no servidor' + servidor + '!'
			});
		});


		irc_client.addListener('join', function(canal, nick){
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": nick,
				"msg": 'Beeem Vindo ' + nick + 'ao seu novo canal' + canal '!'
			});
			console.log('O usuario %s acaba de entrar no canal %s', nick, canal);
		});


		irc_client.addListener('kick', function(canal, nick, by, reason){
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": by,
				"msg": 'Você esta excluíndo o usuário ' + nick + ' do canal ' + canal
			});
			console.log('Desculpe usuário de nick '+ nick + ' você acabou de ser excluído'
				+ 'do canal ' canal + ' pelo usuário ' + by ' pela razao ' + reason);
		})

		irc_client.addListener('names', function(canal, nicks){
			enviarParaCliente(id, {
				"timestamp": Date.now(),
				"nick": nick,
				"msg": 'Nicks dos usuários do canal: ' + JSON.stringify(nicks)
			});
			console.log('Nicks dos usuários do canal: ', irc_client.nicks[canal]);
		});


		irc_client.addListener('motd', function(message) {
		    console.log('motd: ', message);
		    enviarParaCliente(id, {
		    	"timestamp": Date.now(), 
				"nick": nick,
				"msg": message
			});
		});

		irc_client.addListener('error', function(message) {
		    console.log('error: ', message);
		    enviarParaCliente(id, {
		    	"timestamp": Date.now(),
		    	"nick": 'Servidor IRC',
		    	"msg": 'ATENÇÃO:' + message '!!!'
		    });
		});	


		proxies[id] = irc_client;
	});

	receberDoCliente("gravar_mensagem", function (msg) {
		
		if (msg.msg.charAt(0) == '/'){
			if (msg.msg == "/join"){
				if (irc_client.channels[cmd.split(" ")[1]]){
					irc_client.send("error");
				}else{
					irc_client.channels.push(cmd.split(" ")[1]);
					irc_client.join(cmd.split(" ")[1]);
				}
			}
			if (msg.msg == "/nick"){

			}
			if (msg.msg == "/names"){
				var usuario_do_canal = irc_client.opt.channels[0];
				irc_client.send("names", user_canal);
			}
			if (msg.msg == "/motd"){
				irc_client.send("motd");
			}
			if (msg.msg == "/quit"){
				irc_client.send("quit");
				irc_client.disconnect();
				enviarParaCliente(irc_client.id, '/quitOk');
				delete proxies[irc_client.id];
				delete irc_client;
			}
		}

		console.log(msg);
		irc_client.say(msg.canal, msg.msg);
	});
}

function receberDoCliente (canal, callback) {

	amqp_ch.assertQueue(canal, {durable: false});

    console.log(" [irc] Waiting for messages in ", canal);
    
    amqp_ch.consume(canal, function(msg) {

      console.log(" [irc] Received %s", msg.content.toString());
      callback(JSON.parse(msg.content.toString()));

    }, {noAck: true});
}

function enviarParaCliente (id, msg) {

	msg = new Buffer(JSON.stringify(msg));

	amqp_ch.assertQueue("user_"+id, {durable: false});
	amqp_ch.sendToQueue("user_"+id, msg);
	console.log(" [irc] Sent %s", msg);
}



