<h1 align="center">Projeto 3 - Grupo 2</h1>
<h1 align="center"> Protocolo AMQP</h1>

<h3>Integrantes</h3>
- Bruno Pidde
- Newton Junior
- Rhandy Mendes
- Felipe Neves
- Rhuan Bruno
- Debora Santos

<h3>1. Especificação</h3>
O objetivo do projeto 3 é desenvolver um sistema de troca de mensagens multiprotocolo, conforme o esboço da arquitetura, com as seguintes características/funcionalidades:

- Uso do protocolo AMQP para troca de mensagens entre produtores e consumidores;

- Uso de uma interface Web para comunicação multi-chat, para envio e recebimento de mensgens AMQP;

- Os canais de comunicação serão expresso através de URLs, por exemplo:

irc://fulano@irc.freenode.net:6667/#ad-si-2017-1 (protocolo IR, conectado no servidor irc.freenode.net, na port 6667, no canal #ad-si-2017-1 como fulano

tg://fulano:123@ad-si-2017-1?auth=xyz (protocolo Telegram, com usuário fulano utilizando senha '123', no canal ad-si-2017-1 com autenticação 'xyz' (hash id de convite de grupo)

- Será obrigatório desenvolver um proxy AMQP/IRC, que faz a comunicação entre o servidor de fila de mensagens (MQ Server) e o servidor IRC.

- Será opcional (com bônus de 20% cada) de um proxy AMQP/Telegram e/ou cliente de notificações em plataforma android que receba notificações de mensagens novas não lidas.

<h3>2. Aspectos Técnicos</h3>
Como servidor de fila mensagens (message broker), será utililzado o RabbitMQ, que suporta o protocolo AMQP (Advanced Message Queue Protocol).

https://www.rabbitmq.com/devtools.html

A plataforma Android suporta cliente AMQP:

https://www.cloudamqp.com/blog/2014-10-28-rabbitmq-on-android.html

Há várias APIs para criação de chatbots para o Telegram:

https://www.google.com.br/search?q=nodejs+telegram

<h3>3. Arquitetura Geral</h3>
![Alt Text](https://uploaddeimagens.com.br/images/000/932/874/full/Arquitetura-Projeto3.png?1496080202)
<h3>4. Documentação</h3>
(em construção)